<?php $__env->startSection("head"); ?>
    <link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
    <link rel="stylesheet"
          href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
    <link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/tables/datatable.css">

    <link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.css">

    <link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/uikit/modals.css">
    <style>
        .btn {
            margin-right: 2.5px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>

    <!-- Page -->
    <div class="page">
        <div class="page-header">
            <h1 class="page-title">Data Kategori Buku</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo e(base_url('panel')); ?>">Dashboard</a>
                </li>
                <li class="breadcrumb-item">Master Data</li>
                <li class="breadcrumb-item active">Kategori Buku</li>
            </ol>
        </div>

        <div class="page-content">
            <!-- Panel Basic -->
            <div class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <button data-target="#modAdd" data-toggle="modal" type="button"
                                class="btn btn-block btn-primary">
                            <i class="icon wb-plus"></i> Tambah Kode
                        </button>
                    </div>
                    <h3 class="panel-title">Daftar Kode Kategori</h3>
                </header>
                <div class="panel-body">
                    <table id="tb_ketegori_buku" class="table table-hover dataTable table-striped w-full">
                        <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Departement</th>
                            <th>Class</th>
                            <th>Subclass</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Kode</th>
                            <th>Departement</th>
                            <th>Class</th>
                            <th>Subclass</th>
                            <th>Aksi</th>
                        </tr>
                        </tfoot>
                        <tbody id="data_table">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End Panel Basic -->

        </div>
    </div>
    <!-- End Page -->

    <!-- Modal Add Kategori Buku-->
    <div class="modal fade" id="modAdd" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="exampleFormModalLabel">Tambah Kode Kategori</h4>
                </div>
                <div class="modal-body">
                    <form id="form-add" class="form-horizontal" method="post" enctype="multipart/form-data"
                          onsubmit="return false;">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Kode : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="kode_kategori" placeholder="Kode Kategori"
                                       required oninvalid="this.setCustomValidity('Kode katagori tidak boleh kosong') "
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Departement : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="departement" placeholder="Departement"
                                       required oninvalid="this.setCustomValidity('Departement tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Class :</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="class" placeholder="Class" required
                                       oninvalid="this.setCustomValidity('Class tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Subclass : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="subclass" placeholder="Subclass" required
                                       oninvalid="this.setCustomValidity('Subclass tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group text-right row">
                            <div class="col-md-9 offset-md-3">
                                <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                                <button class="btn btn-primary" type="submit">Tambahkan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Kategori Buku-->

    <!-- Modal Edit Kategori Buku-->
    <div class="modal fade" id="modEdit" aria-hidden="false" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="exampleFormModalLabel">Edit Kode Kategori</h4>
                </div>
                <div class="modal-body">
                    <form id="form-edit" class="form-horizontal" method="post" enctype="multipart/form-data"
                          onsubmit="return false;">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Kode : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="kode_kategori2" placeholder="Kode Kategori"
                                       required oninvalid="this.setCustomValidity('Kode kategori tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Departement : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="departement2" placeholder="Departement"
                                       required oninvalid="this.setCustomValidity('Departement tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Class :</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="class2" placeholder="Class" required
                                       oninvalid="this.setCustomValidity('Class tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label">Subclass : </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="subclass2" placeholder="Subclass" required
                                       oninvalid="this.setCustomValidity('Subclass tidak boleh kosong')"
                                       oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form-group text-right row">
                            <div class="col-md-9 offset-md-3">
                                <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Kategori Buku-->

<?php $__env->stopSection(); ?>


<?php $__env->startSection("footer"); ?>
    <!-- Plugins -->
    <script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net/jquery.dataTables.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>

    <script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.js"></script>

    <!-- Page -->
    <script src="<?php echo e(assets_back()); ?>global/js/Plugin/datatables.js"></script>
    <script src="<?php echo e(assets_back()); ?>base/assets/examples/js/tables/datatable.js"></script>

    <script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootbox.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-sweetalert.js"></script>
    <script src="<?php echo e(assets_back()); ?>global/js/Plugin/toastr.js"></script>
    <script src="<?php echo e(assets_back()); ?>base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>

    <script>
        var table;


        // *onready
        $(document).ready(function () {
            refresh_table();
        });

        // *refresh table
        function refresh_table() {

            $.get('<?php echo e(base_url()); ?>panel/kategori_buku/get', function (data) {

                if (table) table.clear();

                table = $('#tb_ketegori_buku').DataTable({
                    destroy: true,
                    paging: true,
                    searching: true
                });

                $.each(data['data'], function (index, value) {

                    table.row.add([
                        value['kode_kategori'],
                        value['departement'],
                        value['class'],
                        value['subclass'],
                        '<a onclick="edit(' + value['id_kategori_buku'] + ')" data-target="#modEdit" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"  data_id="' + value['id_kategori_buku'] + '" data-toggle="tooltip" data-original-title="Edit">\n' +
                        '<i class="icon wb-edit" aria-hidden="true"></i> </a>' + '' +
                        '<a onclick="confirmDelete(' + value['id_kategori_buku'] + ')" data-toggle="tooltip" data-original-title="Remove" class="btn btn-sm btn-icon btn-pure btn-default">' +
                        '<i class="icon wb-trash" aria-hidden="true"></i></a>'
                    ]).draw();

                });
            });
        }


        // *create
        $('#form-add').submit(function () {

            $.post('<?php echo e(base_url()); ?>panel/kategori_buku/add', {
                    kode_kategori: $('#kode_kategori').val(),
                    departement: $('#departement').val(),
                    class: $('#class').val(),
                    subclass: $('#subclass').val()
                },
                function (data) {

                    refresh_table();

                    if (data.status == 1) {
                        swal({
                                title: "Berhasil!",
                                text: "Data telah ditambahkan!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: true
                            },
                            function () {
                                clear_input('form-add');
                            });
                    }
                    else {
                        swal("Gagal!", "Coba Periksa Lagi", "error");
                    }

                }).fail(function () {
                swal("Gagal!", "Kode Kategori sudah ada", "error");
            });
        });

        // *edit
        function edit(id) {
            $.get('<?php echo e(base_url()); ?>panel/kategori_buku/edit/' + id,
                function (data) {

                    refresh_table();

                    if (data.status == 1) {
                        $('#kode_kategori2').val(data['data'][0]['kode_kategori']);
                        $('#departement2').val(data['data'][0]['departement']);
                        $('#class2').val(data['data'][0]['class']);
                        $('#subclass2').val(data['data'][0]['subclass']);
                        id_update = id;
                    }
                    else {
                        swal("Gagal!", "Coba Periksa Lagi", "error");
                    }

                });
        }

        // #update
        $('#form-edit').submit(function () {

            $.post('<?php echo e(base_url()); ?>panel/kategori_buku/update/' + id_update, {
                    kode_kategori: $('#kode_kategori2').val(),
                    departement: $('#departement2').val(),
                    class: $('#class2').val(),
                    subclass: $('#subclass2').val()
                },
                function (data) {

                    refresh_table();

                    if (data.status == 1) {

                        swal({
                                title: "Berhasil!",
                                text: "Data telah di update!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: true
                            },
                            function () {
                                $('#modEdit').modal('hide');
                            });
                    }
                    else {
                        swal("Gagal!", "Coba Periksa Lagi", "error");
                    }

                });
        });

        // *delete
        function confirmDelete(id) {

            swal({
                title: "Data Kategori Buku",
                text: "Apa anda yakin ingin menghapus data ini?",
                type: "warning",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus Data",
                cancelButtonText: "Batalkan",
                closeOnConfirm: false
            }, function () {

                $.post('<?php echo e(base_url()); ?>panel/kategori_buku/delete/' + id, function (data) {

                    if (data.status === 1) {

                        refresh_table();

                        swal({
                                title: "Berhasil!",
                                text: "Data telah dihapus!",
                                type: "success",
                                showCloseButton: true,
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: true
                            },
                            function () {
                                clear_input('form-add');
                            });
                    }
                    else {
                        refresh_table();
                        swal("Gagal!", "Coba Periksa Lagi", "error");
                    }

                });
            });
        }
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>