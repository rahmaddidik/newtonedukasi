<?php $__env->startSection("head"); ?>
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/uikit/modals.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>

<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Data Relasi</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo e(base_url('panel')); ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Master Data</li>
            <li class="breadcrumb-item active">Data Relasi</li>
        </ol>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button data-target="#modAddRelasi" data-toggle="modal" type="button" class="btn btn-block btn-primary">
                        <i class="icon wb-plus"></i> Tambah Relasi</button>
                </div>
                <h3 class="panel-title">Daftar Relasi</h3>
            </header>
            <div class="panel-body">
                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                    <thead>
                        <tr>
                            <th rowspan="2">General (GM)</th>
                            <th rowspan="2">Nama Relasi</th>
                            <th rowspan="2">No Hp</th>
                            <th rowspan="2">Email</th>
                            <th rowspan="2">Tgl Bergabung</th>
                            <th colspan="4" class="text-center">Diskon</th>
                            <th rowspan="2">Aksi</th>
                        </tr>
                        <tr>
                            <th>DTP</th>
                            <th>BKP</th>
                            <th>ALQURAN</th>
                            <th>KHUSUS</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>General (GM)</th>
                            <th>Nama Relasi</th>
                            <th>No Hp</th>
                            <th>Email</th>
                            <th>Tgl Bergabung</th>
                            <th>DTP</th>
                            <th>BKP</th>
                            <th>ALQURAN</th>
                            <th>KHUSUS</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>Forum Edukasi</td>
                            <td>Forum Edukasi</td>
                            <td>082136116811</td>
                            <td>info@forumedukasi.org</td>
                            <td>19/06/2018</td>
                            <td>20%</td>
                            <td>18%</td>
                            <td>35%</td>
                            <td>40%</td>
                            <td>
                                <a data-target="#modEditRelasi" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                    data-toggle="tooltip" data-original-title="Edit">
                                    <i class="icon wb-edit" aria-hidden="true"></i>
                                </a>
                                <a href="#" id="exampleWarningConfirm" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip"
                                    data-original-title="Remove">
                                    <i class="icon wb-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Panel Basic -->

    </div>
</div>
<!-- End Page -->

<!-- Modal Add Relasi-->
<div class="modal fade" id="modAddRelasi" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Tambah Relasi</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="kode_relasiq" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">General (GM) : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="gm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Nama Relasi : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="relasi" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Hp :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="hp" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Email : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Rekening :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="rekening" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Tgl Bergabung : </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" placeholder="">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Diskon :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="kode_diskon" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add Relasi-->

<!-- Modal Edit Relasi-->
<div class="modal fade" id="modEditRelasi" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Edit Relasi</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="kode_relasi" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">General (GM) : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="gm" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Nama Relasi : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="relasi" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Hp :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="hp" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Email : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Rekening :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="rekening" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Tgl Bergabung : </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" value="17/06/2018">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Diskon :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="kode_diskon" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalEditSuccess">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Relasi-->

<?php $__env->stopSection(); ?>

    
<?php $__env->startSection("footer"); ?>
<!-- Plugins -->
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>

<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.js"></script>

<script src="<?php echo e(assets_back()); ?>global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Page -->
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/datatables.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/tables/datatable.js"></script>

<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/jquery-placeholder.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/input-group-file.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-datepicker.js"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>