<?php $__env->startSection("head"); ?>
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/uikit/modals.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>

<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Transaksi Retur Masuk</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
            <a href="<?php echo e(base_url('panel')); ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Transaksi</li>
            <li class="breadcrumb-item active">Retur Masuk</li>
        </ol>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button data-target="#modAddRetur" data-toggle="modal" type="button" class="btn btn-block btn-primary">
                        <i class="icon wb-plus"></i> Tambah Retur Masuk</button>
                </div>
                <h3 class="panel-title">Daftar Retur Masuk</h3>
            </header>
            <div class="panel-body">
                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                    <thead>
                        <tr>
                            <th>ID Orin</th>
                            <th>Tanggal</th>
                            <th>Judul Buku</th>
                            <th>KD Relasi</th>
                            <th>No Faktur</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID Orin</th>
                            <th>Tanggal</th>
                            <th>Judul Buku</th>
                            <th>KD Relasi</th>
                            <th>No Faktur</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>207975330</td>
                            <td>22/05/2018</td>
                            <td>25 Desain Rumah Mungil 1 Lantai</td>
                            <td>REL1726</td>
                            <td>FAK214</td>
                            <td>200</td>
                            <td>Selesai</td>
                            <td>
                                <a data-target="#modEditRetur" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="tooltip"
                                    data-original-title="Edit">
                                    <i class="icon wb-edit" aria-hidden="true"></i>
                                </a>
                                <a href="#" id="exampleWarningConfirm" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip"
                                    data-original-title="Remove">
                                    <i class="icon wb-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Panel Basic -->

    </div>
</div>
<!-- End Page -->

<!-- Modal Add Retur-->
<div class="modal fade" id="modAddRetur" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Tambah Retur Masuk</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ID ORIN: </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="id_orin" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">TANGGAL: </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" placeholder="">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Relasi : </label>
                        <div class="col-md-9">
                            <select class="form-control" name="kd_relasi">
                                <option value="REL827">REL827</option>
                                <option value="REL167">REL167</option>
                                <option value="REL281">REL281</option>
                                <option value="REL142">REL142</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Faktur : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="no_faktur" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Keterangan : </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="ket" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Jumlah : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jumlah" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add Retur-->

<!-- Modal Edit Retur-->
<div class="modal fade" id="modEditRetur" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Edit Retur Masuk</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ID ORIN: </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="id_orin" value="207975330">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">TANGGAL: </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" value="17/06/2018">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Relasi : </label>
                        <div class="col-md-9">
                            <select class="form-control" name="kd_relasi">
                                <option value="REL827">REL827</option>
                                <option value="REL167">REL167</option>
                                <option value="REL281" selected>REL281</option>
                                <option value="REL142">REL142</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Faktur : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="no_faktur" value="FAK72617">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Keterangan : </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="ket" placeholder="">Selesai</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Jumlah : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jumlah" value="400">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalEditSuccess">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Retur-->

<?php $__env->stopSection(); ?>

    
<?php $__env->startSection("footer"); ?>
<!-- Plugins -->
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>

<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.js"></script>

<script src="<?php echo e(assets_back()); ?>global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Page -->
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/datatables.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/tables/datatable.js"></script>

<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/jquery-placeholder.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/input-group-file.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-datepicker.js"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>