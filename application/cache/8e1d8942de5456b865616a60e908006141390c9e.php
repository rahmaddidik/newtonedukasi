<?php $__env->startSection("head"); ?>
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/uikit/modals.css">

<style>
  #showPass,
  #showPass1 {
    position: absolute;
    top: 6px;
    right: 30px;
  }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>

<!-- Page -->
<div class="page">
  <div class="page-header">
    <h1 class="page-title">Data Users</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?php echo e(base_url('panel')); ?>">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Users</li>
    </ol>
  </div>

  <div class="page-content">
    <!-- Panel Basic -->
    <div class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
          <button data-target="#modAddUser" data-toggle="modal" type="button" class="btn btn-block btn-primary">
            <i class="icon wb-plus"></i> Tambah Users</button>
        </div>
        <h3 class="panel-title">Daftar Users</h3>
      </header>
      <div class="panel-body">
        <table id="tb_users" class="table table-hover dataTable table-striped w-full">
          <thead>
            <tr>
              <th>Tgl</th>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Email</th>
              <th>No Hp</th>
              <th>Alamat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Tgl</th>
              <th>Jabatan</th>
              <th>Email</th>
              <th>No Hp</th>
              <th>Alamat</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
    <!-- End Panel Basic -->

  </div>
</div>
<!-- End Page -->

<!-- Modal Add User-->
<div class="modal fade" id="modAddUser" aria-hidden="false" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="exampleFormModalLabel">Tambah Users</h4>
      </div>
      <div class="modal-body" >
        <form id="form-add" class="form-horizontal"  class="form-horizontal" method="post" enctype="multipart/form-data"
              onsubmit="return false;">
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Nama : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="nama">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Jabatan : </label>
            <div class="col-md-9">
              <select class="form-control" id="jabatan">
                <option value="1">Owner</option>
                <option value="2">Karyawan</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">email : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="email">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">password : </label>
            <div class="col-md-9">
              <input id="password" type="password" class="form-control">
              <i id="showPass" class="fa fa-eye"></i>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">No Hp : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="hp">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Alamat : </label>
            <div class="col-md-9">
              <textarea class="form-control" rows="3" id="alamat"></textarea>
            </div>
          </div>
          <div class="form-group row text-right">
            <div class="col-md-9 offset-md-3">
              <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
              <button class="btn btn-primary" type="submit">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Add User-->

<!-- Modal Edit User-->
<div class="modal fade" id="modEditUser" aria-hidden="false" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="exampleFormModalLabel">Edit Users</h4>
      </div>
      <div class="modal-body">
        <form id="form-edit" class="form-horizontal" method="post" enctype="multipart/form-data"
              onsubmit="return false;">
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Nama : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="nama2" >
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Jabatan : </label>
            <div class="col-md-9">
              <select class="form-control" id="jabatan2">
                <option value="1" selected>Owner</option>
                <option value="2">Karyawan</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Email : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="email2" >
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Ganti password : </label>
            <div class="col-md-9">
              <input id="password2" type="password" class="form-control">
              <i id="showPass1" class="fa fa-eye"></i>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">No Hp : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="hp2">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Alamat : </label>
            <div class="col-md-9">
              <textarea class="form-control" rows="3" id="alamat2">

              </textarea>
            </div>
          </div>
          <div class="form-group row text-right">
            <div class="col-md-9 offset-md-3">
              <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
              <button class="btn btn-primary" type="submit">Update</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Edit User-->

<?php $__env->stopSection(); ?>

    
<?php $__env->startSection("footer"); ?>
<!-- Plugins -->
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>

<!-- Page -->
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/datatables.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/tables/datatable.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script>
    var table;
    var data_ip;
    var hari = ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"];
    var bulan =["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    var bulan_limit =["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des"];
    // *onready
    $(document).ready(function () {
        refresh_table();
        cekip();
    });
     function cekip() {

        $.getJSON('https://ipapi.co/json/',function (data) {
            data_ip=data['ip'];
        });
    }
    // *refresh table
    function refresh_table() {

        $.get('<?php echo e(base_url()); ?>panel/users/get', function (data) {

            if (table) table.clear();

            table = $('#tb_users').DataTable({
                destroy: true,
                paging: true,
                searching: true,
                "order": [[ 1, "asc" ]]
            });
            var id_data=0;
            $.each(data['data'], function (index, value) {
                myDate = new Date(1000* value['created_on']);
                myDate =   myDate.getDate()+' - '+bulan_limit[myDate.getMonth()]+' - '+myDate.getFullYear();
                if (value['id']>id_data){
                    table.row.add([
                        myDate,
                        value['name'],
                        value['description'],
                        value['email'],
                        value['phone'],
                        value['address'],
                        '<a onclick="edit(' + value['id'] + ')" data-target="#modEditUser" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"  data_id="' +value['id']+ '" data-toggle="tooltip" data-original-title="Edit">\n' +
                        '<i class="icon wb-edit" aria-hidden="true"></i> </a>' + '' +
                        '<a onclick="confirmDelete(' +value['id']+ ')" data-toggle="tooltip" data-original-title="Remove" class="btn btn-sm btn-icon btn-pure btn-default">' +
                        '<i class="icon wb-trash" aria-hidden="true"></i></a>'
                    ]).draw();

                }
                id_data=value['id'];
            });
        });
    }

    // *create
    $('#form-add').submit(function () {

        $.post('<?php echo e(base_url()); ?>panel/users/add', {
                jabatan:$('#jabatan').val(),
                username: $('#email').val(),
                password:$('#password').val(),
                email: $('#email').val(),
                name: $('#nama').val(),
                phone: $('#hp').val(),
                address: $('#alamat').val(),
            },
            function (data) {

                refresh_table();

                if (data.status == 1) {
                    swal({
                            title: "Berhasil!",
                            text: "Data telah ditambahkan!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            clear_input('form-add');
                        });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
    });

    // *edit
    function edit(id) {
        $.get('<?php echo e(base_url()); ?>panel/users/edit/' + id,
            function (data) {

                refresh_table();

                if (data.status == 1) {
                    $('#nama2').val(data['data'][0]['name']);
                    $('#jabatan2').val(data['data'][0]['description']);
                    $('#email2').val(data['data'][0]['email']);
                    $('#hp2').val(data['data'][0]['phone']);
                    $('#alamat2').val(data['data'][0]['address']);
                    id_update = id;
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
    }

    // #update
    $('#form-edit').submit(function () {

        $.post('<?php echo e(base_url()); ?>panel/users/update/' + id_update, {
                jabatan:$('#jabatan2').val(),
                name: $('#nama2').val(),
                description: $('#jabatan2').val(),
                email: $('#email2').val(),
                phone: $('#hp2').val(),
                address: $('#alamat2').val(),
                password: $('#password2').val()
            },
            function (data) {
                console.log(data);
                refresh_table();

                if (data.status == 1) {

                    swal({
                            title: "Berhasil!",
                            text: "Data telah di update!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            $('#modEdit').modal('hide');
                        });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
    });

    // *delete
    function confirmDelete(id) {

        swal({
            title: "Data Kategori Buku",
            text: "Apa anda yakin ingin menghapus data ini?",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data",
            cancelButtonText: "Batalkan",
            closeOnConfirm: false
        }, function () {

            $.post('<?php echo e(base_url()); ?>panel/users/delete/' + id, function (data) {

                if (data.status === 1) {

                    refresh_table();

                    swal({
                            title: "Berhasil!",
                            text: "Data telah dihapus!",
                            type: "success",
                            showCloseButton: true,
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            clear_input('form-add');
                        });
                }
                else {
                    refresh_table();
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
        });
    }



    $( "#showPass" )
    .mouseup(function() {
        $("#password").attr('type','password');
    })
    .mousedown(function() {
        $("#password").attr('type','text');
    });
    $( "#showPass1" )
    .mouseup(function() {
        $("#password2").attr('type','password');
    })
    .mousedown(function() {
        $("#password2").attr('type','text');
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>