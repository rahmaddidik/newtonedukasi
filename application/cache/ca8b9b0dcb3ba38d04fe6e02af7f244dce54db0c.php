<?php $__env->startSection("head"); ?>
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/uikit/modals.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/pages/invoice.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>

<!-- Page -->
<div class="page">
    <div class="hide-print page-header">
        <h1 class="page-title">Faktur Keluar BKP Gramedia</h1>
    </div>

    <div class="page-content">
        <!-- Panel -->
        <div class="panel">
            <form class="form-horizontal" action="http://localhost/cvrds/newtonedukasi/panel/faktur_bkp">
                <div class="panel-body container-fluid">
                    <div class="row">
                        <div class="col-sm-4">
                            <h3>
                                <img class="mr-10" height="50" src="<?php echo e(assets_back()); ?>base/assets/images/logo_color.png" alt="...">
                            </h3>
                            <address>
                                <abbr>Relasi &nbsp;&nbsp;&nbsp; : </abbr> &nbsp;&nbsp; CVRDS OFFICIAL
                                <br>
                                <abbr>Alamat &nbsp; : </abbr> &nbsp;&nbsp; Blali RT005, Seloharjo, Pundong, Bantul, Yogyakarta 55771
                            </address>
                        </div>
                        <div class="col-sm-4 offset-sm-4 text-right">
                            <h4>No Faktur : <a href="">#5669626</a></h4>
                            <address class="mt-30">
                                <abbr>Kode Relasi &nbsp; : </abbr> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; KR26172
                                <br>
                                <abbr>Tgl Faktur &nbsp; : </abbr> &nbsp;&nbsp; 13 Juni 2019
                                <br>
                                <abbr>Pajak &nbsp; : </abbr> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BKP
                                <br>
                            </address>
                        </div>
                    </div>

                    <div class="page-invoice-table table-responsive mt-30">
                        <table class="table table-hover text-right">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Orin</th>
                                    <th>Judul Buku</th>
                                    <th>Harga Jual</th>
                                    <th>Diskon</th>
                                    <th>Jumlah</th>
                                    <th>Nilai Jual</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>A025</td>
                                    <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                    <td>Rp159.800</td>
                                    <td>35%</td>
                                    <td>100</td>
                                    <td>Rp11.980.000</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>A025</td>
                                    <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                    <td>Rp159.800</td>
                                    <td>35%</td>
                                    <td>100</td>
                                    <td>Rp11.980.000</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>A025</td>
                                    <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                    <td>Rp159.800</td>
                                    <td>35%</td>
                                    <td>100</td>
                                    <td>Rp11.980.000</td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="text-right clearfix">
                        <div class="float-right">
                            <p>Bruto:
                                <span>Rp33.598.000</span>
                            </p>
                            <p>Diskon:
                                <span>Rp2.500.000</span>
                            </p>
                            <p class="page-invoice-amount">Netto:
                                <span>Rp30.098.000</span>
                            </p>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <p>Yogyakarta,.....................................2019</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <p>Administrasi</p>
                                <br><br><br>
                                <p>(..........................................)</p>
                            </div>
                            <div class="col-md-3 text-center">
                                <p>mengetahui</p>
                                <br><br><br>
                                <p>(..........................................)</p>
                            </div>
                        </div>
                    </div>

                    <div class="hide-print text-right">
                        <button type="submit" class="btn btn-animate btn-animate-side btn-primary">
                            <span>
                                <i class="icon wb-shopping-cart" aria-hidden="true"></i> Edit Faktur</span>
                        </button>
                        <button type="button" class="btn btn-animate btn-animate-side btn-default btn-outline" onclick="javascript:window.print();">
                            <span>
                                <i class="icon wb-print" aria-hidden="true"></i> Cetak</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Page -->

<!-- Modal Add Retur-->
<div class="modal fade" id="modAddFaktur" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Tambah Faktur BKP</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ID ORIN: </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="id_orin" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Jumlah : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jumlah" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add Retur-->

<!-- Modal Edit Retur-->
<div class="modal fade" id="modEditFaktur" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Edit Retur Masuk</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ID ORIN: </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="id_orin" value="207975330">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">TANGGAL: </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" value="17/06/2018">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Relasi : </label>
                        <div class="col-md-9">
                            <select class="form-control" name="kd_relasi">
                                <option value="REL827">REL827</option>
                                <option value="REL167">REL167</option>
                                <option value="REL281" selected>REL281</option>
                                <option value="REL142">REL142</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Faktur : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="no_faktur" value="FAK72617">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Keterangan : </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="ket" placeholder="">Selesai</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Jumlah : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jumlah" value="400">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalEditSuccess">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Retur-->

<?php $__env->stopSection(); ?>

    
<?php $__env->startSection("footer"); ?>
<!-- Plugins -->
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>

<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.js"></script>

<script src="<?php echo e(assets_back()); ?>global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Page -->
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/datatables.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/tables/datatable.js"></script>

<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/jquery-placeholder.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/input-group-file.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-datepicker.js"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>