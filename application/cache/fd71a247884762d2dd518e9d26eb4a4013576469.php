<?php $__env->startSection("head"); ?>
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="<?php echo e(assets_back()); ?>base/assets/examples/css/uikit/modals.css">

<style>
  #showPass,
  #showPass1 {
    position: absolute;
    top: 6px;
    right: 30px;
  }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("content"); ?>

<!-- Page -->
<div class="page">
  <div class="page-header">
    <h1 class="page-title">Data Users</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?php echo e(base_url('panel')); ?>">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Users</li>
    </ol>
  </div>

  <div class="page-content">
    <!-- Panel Basic -->
    <div class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
          <button data-target="#modAddUser" data-toggle="modal" type="button" class="btn btn-block btn-primary">
            <i class="icon wb-plus"></i> Tambah Users</button>
        </div>
        <h3 class="panel-title">Daftar Users</h3>
      </header>
      <div class="panel-body">
        <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Email</th>
              <th>No Hp</th>
              <th>Alamat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Email</th>
              <th>No Hp</th>
              <th>Alamat</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
          <tbody>
            <tr>
              <td>Admin</td>
              <td>Owner</td>
              <td>admin@newtonedukasi.com</td>
              <td>082136116811</td>
              <td>Blali Seloharjo Pundong</td>
              <td>
                <a data-target="#modEditUser" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="tooltip"
                  data-original-title="Edit">
                  <i class="icon wb-edit" aria-hidden="true"></i>
                </a>
                <a href="#" id="exampleWarningConfirm" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip"
                  data-original-title="Remove">
                  <i class="icon wb-trash" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- End Panel Basic -->

  </div>
</div>
<!-- End Page -->

<!-- Modal Add User-->
<div class="modal fade" id="modAddUser" aria-hidden="false" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="exampleFormModalLabel">Tambah Users</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Nama : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="nama">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Jabatan : </label>
            <div class="col-md-9">
              <select class="form-control" name="jabatan">
                <option value="owner">Owner</option>
                <option value="karyawan">Karyawan</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">email : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="email">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">password : </label>
            <div class="col-md-9">
              <input id="password" type="password" class="form-control">
              <i id="showPass" class="fa fa-eye"></i>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">No Hp : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="hp">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Alamat : </label>
            <div class="col-md-9">
              <textarea class="form-control" rows="3"></textarea>
            </div>
          </div>
          <div class="form-group row text-right">
            <div class="col-md-9 offset-md-3">
              <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
              <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Add User-->

<!-- Modal Edit User-->
<div class="modal fade" id="modEditUser" aria-hidden="false" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="exampleFormModalLabel">Edit Users</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Nama : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="nama" value="Admin">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Jabatan : </label>
            <div class="col-md-9">
              <select class="form-control" name="jabatan">
                <option value="owner" selected>Owner</option>
                <option value="karyawan">Karyawan</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Email : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="email" value="admin@newtonedukasi.com">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Ganti password : </label>
            <div class="col-md-9">
              <input id="password1" type="password" class="form-control">
              <i id="showPass1" class="fa fa-eye"></i>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">No Hp : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="hp" value="082136116811">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Alamat : </label>
            <div class="col-md-9">
              <textarea class="form-control" rows="3">
                Blali RT05 Seloharjo Pundong Bantul
              </textarea>
            </div>
          </div>
          <div class="form-group row text-right">
            <div class="col-md-9 offset-md-3">
              <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
              <button class="btn btn-primary" type="button" id="swalEditSuccess">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Edit User-->

<?php $__env->stopSection(); ?>

    
<?php $__env->startSection("footer"); ?>
<!-- Plugins -->
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>

<!-- Page -->
<script src="<?php echo e(assets_back()); ?>global/vendor/bootbox/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/vendor/toastr/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/datatables.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/tables/datatable.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootbox.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="<?php echo e(assets_back()); ?>global/js/Plugin/toastr.js"></script>
<script src="<?php echo e(assets_back()); ?>base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script>
    $( "#showPass" )
    .mouseup(function() {
        $("#password").attr('type','password');
    })
    .mousedown(function() {
        $("#password").attr('type','text');
    });
    $( "#showPass1" )
    .mouseup(function() {
        $("#password1").attr('type','password');
    })
    .mousedown(function() {
        $("#password1").attr('type','text');
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>