<?php 
require 'vendor/autoload.php';

use Philo\Blade\Blade;

if (! function_exists('view')) {

    /**
     * Get blade view
     *
     * @param $view
     * @param array $data
     * @return mixed
     */
    function view($view, $data = [],$dir)
    {
        $view_blade=str_replace_first('controllers','views',realpath($dir));
        $cache=FCPATH.'application/cache';
        $blade= new Blade($view_blade,$cache);
        echo $blade->view()->make($view, $data)->render();
    }
}

if (! function_exists('view_back')) {

    /**
     * Get blade view
     *
     * @param $view
     * @param array $data
     * @return mixed
     */
    function view_back($view, $data = [])
    {
        $blade = new \Philo\Blade\Blade( FCPATH.'application/modules/', FCPATH.'application/cache');
        echo $blade->view()->make($view, $data)->render();
    }
}

if (! function_exists('view_front')) {

    /**
     * Get blade view
     *
     * @param $view
     * @param array $data
     * @return mixed
     */
    function view_front($view, $data = [])
    {
        $blade = new \Philo\Blade\Blade( FCPATH.'application/modules/default/views', FCPATH.'application/cache');
        echo $blade->view()->make($view, $data)->render();
        exit();
    }
}

if(!function_exists('assets_front')){
    
    function assets_front(){
        return base_url('desain/front-end/');
    }

}

if(!function_exists('assets_back')){
    
    function assets_back(){
        return base_url('desain/back-end/');
    }

}

$this->load->helper('form');
$this->load->library('parser');
$this->load->helper(array('url','download'));