<?php
class  Auth extends CI_Model {

    protected $user;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function is_logged_in() {
        return $this->session->userdata('id');
    }

    public function login($email, $password){
        $this->db->where('email', $email);
        $this->db->where('password', sha1($password));
        return $this->db->get('tb_admin');
    }

    public function check(){
        if($this->is_logged_in() == false){
            redirect('panel/auth');
        }
    }

    public function data(){
        $data   = $this->session->all_userdata();
        echo json_encode($data);
        exit();
    }

    public function user(){
        if( ! $this->is_logged_in() ){
            return;
        }
        if( ! is_null($this->user) ){
            return $this->user;
        }

        $this->db->where('id_admin', $this->session->userdata('id') );
        return $this->user = $this->db->get('tb_admin')->result()[0];
    }

    public function superAdmin(){
        return $this->user()->status == 'super_admin';
    }

    public function admin_only(){
        if( ! $this->superAdmin() ){
            header('location: '.base_url().'panel' );
        }
    }

}
?>
