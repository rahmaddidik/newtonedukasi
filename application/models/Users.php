<?php
class  Users extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get()
    {

        $this->db->select('users.id,users.name,groups.description,users.email,users.phone,users.address,users.active,users.created_on');
        $this->db->from('users');
        $this->db->join('users_groups','users_groups.user_id = users.id');
        $this->db->join('groups','groups.id = users_groups.group_id');
        $this->db->group_by('users.id');
        $this->db->order_by('users.id', 'asc');
        $db=$this->db->get();
        return $db;
    }

    function get_by_id($id)
    {
        $this->db->distinct();
        $this->db->select('users.id,users.name,groups.description,users.email,users.phone,users.address,users.active');
        $this->db->from('users');
        $this->db->join('users_groups','users_groups.user_id = users.id');
        $this->db->join('groups','groups.id = users_groups.group_id');
        $this->db->where('users.id',$id);
        $db=$this->db->get();
        return $db;
    }

    function get_by_id_all($id)
    {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('users_groups','users_groups.user_id = users.id');
        $this->db->join('groups','groups.id = users_groups.group_id');
        $this->db->where('users.id',$id);
        $db=$this->db->get();
        return $db;
    }

    function join($table,$condition){
        foreach ($condition as $key => $value){
            if($key == 'distinct' and $value=='true'){
                $this->db->distinct();
            }
            if($key == 'select'){
                $this->db->select($value[0]);
            }
            if($key == 'join'){
                $this->db->join($value[0],$value[1],$value[2]);
            }
            if($key == 'where'){
                // $this->db->join($value[0],$value[1],$value[3]);
            }
        }
        echo "<pre>";

        var_dump($this->db->get($table)->result_array());
        exit();
        return $db=$this->db->get($table);
    }

}