<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>Dashboard Newton Edukasi</title>
    
    <link rel="apple-touch-icon" href="{{assets_back()}}base/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="{{assets_back()}}base/assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{assets_back()}}global/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{assets_back()}}global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="{{assets_back()}}base/assets/css/site.min.css">
    <link rel="stylesheet" href="{{assets_back()}}base/assets/skins/red.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/flag-icon-css/flag-icon.css">
    
    @yield('head')
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="{{assets_back()}}global/fonts/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="{{assets_back()}}global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="{{assets_back()}}global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="{{assets_back()}}global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="{{assets_back()}}global/vendor/media-match/media.match.min.js"></script>
    <script src="{{assets_back()}}global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="{{assets_back()}}global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition ecommerce_dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    @include('sidebar')
    @yield('content')
    @include('footer')
    

    <!-- Core  -->
    <script src="{{assets_back()}}global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="{{assets_back()}}global/vendor/jquery/jquery.js"></script>
    <script src="{{assets_back()}}global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="{{assets_back()}}global/vendor/bootstrap/bootstrap.js"></script>
    <script src="{{assets_back()}}global/vendor/animsition/animsition.js"></script>
    <script src="{{assets_back()}}global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="{{assets_back()}}global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="{{assets_back()}}global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="{{assets_back()}}global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    
    <!-- Plugins -->
    <script src="{{assets_back()}}global/vendor/switchery/switchery.js"></script>
    <script src="{{assets_back()}}global/vendor/intro-js/intro.js"></script>
    <script src="{{assets_back()}}global/vendor/screenfull/screenfull.js"></script>
    <script src="{{assets_back()}}global/vendor/slidepanel/jquery-slidePanel.js"></script>
    
    <!-- Scripts -->
    <script src="{{assets_back()}}global/js/Component.js"></script>
    <script src="{{assets_back()}}global/js/Plugin.js"></script>
    <script src="{{assets_back()}}global/js/Base.js"></script>
    <script src="{{assets_back()}}global/js/Config.js"></script>
    
    <script src="{{assets_back()}}base/assets/js/Section/Menubar.js"></script>
    <script src="{{assets_back()}}base/assets/js/Section/GridMenu.js"></script>
    <script src="{{assets_back()}}base/assets/js/Section/Sidebar.js"></script>
    <script src="{{assets_back()}}base/assets/js/Section/PageAside.js"></script>
    <script src="{{assets_back()}}base/assets/js/Plugin/menu.js"></script>
    
    <script src="{{assets_back()}}global/js/config/colors.js"></script>
    <script src="{{assets_back()}}base/assets/js/config/tour.js"></script>
    <script>Config.set('assets', '{{assets_back()}}base/assets');</script>
    
    <!-- Page -->
    <script src="{{assets_back()}}base/assets/js/Site.js"></script>
    <script src="{{assets_back()}}global/js/Plugin/asscrollable.js"></script>
    <script src="{{assets_back()}}global/js/Plugin/slidepanel.js"></script>
    <script src="{{assets_back()}}global/js/Plugin/switchery.js"></script>
    <script src="{{assets_back()}}custom/js/dataTableControl.js"></script>
    @yield('footer')

    <script>
    // /* Create */
    // $("#form-add").submit(function(e){
    //   e.preventDefault(); //cancel default action
    //   var form_action = $("#modAdd").find("form").attr("action");
    //   var formData = new FormData($(this)[0]);
    //   $.ajax({
    //       cache : false,
    //       dataType: 'json',
    //       url: form_action,
    //       type: "POST",
    //       data: formData,
    //       contentType: false,
    //       async: false,
    //       processData: false
    //   }).done(function(data){
    //     console.log(data);
    //     if (data.status == 1) {
    //       swal({
    //         title: "Berhasil!",
    //         text: "Data telah ditambahkan!",
    //         type: "success",
    //         showCancelButton: false,
    //         confirmButtonClass: "btn-success",
    //         confirmButtonText: 'OK',
    //         closeOnConfirm: false
    //       },
    //       function(){
    //         location.reload();
    //       });
    //     }
    //     else {
    //       swal("Gagal!", "Coba Periksa Lagi", "error");
    //     }
    //   });
    //
    //
    // });
    //
    // /* Delete */
    // $('[data-confirm]').on('click', function(e) {
    //     e.preventDefault(); //cancel default action
    //
    //     //Recuperate href value
    //     var href = $(this).attr('href');
    //     var message = $(this).data('confirm');
    //
    //     //pop up
    //     swal({
    //       title: "Yakin ingin menghapus data?",
    //       text: "data anda akan dihapus secara permanen!",
    //       type: "warning",
    //       showCancelButton: true,
    //       confirmButtonClass: "btn-warning",
    //       confirmButtonText: 'YA, Hapus!',
    //       cancelButtonText: 'Batal',
    //       closeOnConfirm: false
    //       //closeOnCancel: false
    //     })
    //     .then((willDelete) => {
    //         if (willDelete) {
    //             swal({
    //               title: "Terhapus!",
    //               text: "Data telah berhasil dihapus!",
    //               type: "success",
    //               showCancelButton: false,
    //               confirmButtonClass: "btn-success",
    //               confirmButtonText: 'OK',
    //               closeOnConfirm: false
    //             },
    //             function(){
    //               location.reload();
    //             });
    //         } else {
    //             swal("Hapus data dibatalkan");
    //         }
    //     });
    // });
    </script>
    
</body>

</html>
