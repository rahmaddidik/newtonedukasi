<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
        function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url', 'download'));
                $this->load->library('form_validation');
        }
        
	public function index()
	{
                view_front("pages/home",[],__DIR__);
        }

}
