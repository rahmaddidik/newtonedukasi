<!-- NAVBAR FIXED -->
<div class="nav-top bg-primary d-none d-lg-block">
    <div class="container">
        <ul class="nav">
            <li class="nav-item">
                <a href="tel:0275322728" class="nav-link text-white"><i class="fa fa-phone"></i> (0275) 322728</a>
            </li>
            <li class="nav-item">
                <a href="mailto:smkkesehatan.pwr@gmail.com" class="nav-link text-white"><i class="fa fa-envelope-o"></i> smkkesehatan.pwr@gmail.com</a>
            </li>
            <li class="nav-item sosmed ml-auto d-none d-md-block">
                <a target="_blank" href="https://www.facebook.com/SMK-Kesehatan-Purworejo-656055987819159"><i class="fa fa-facebook"></i></a>
            </li>
            <li class="nav-item sosmed d-none d-md-block">
                <a target="_blank" href="https://www.instagram.com/officialsmkkespwr/"><i class="fa fa-instagram"></i></a>
            </li>
            <li class="nav-item sosmed d-none d-md-block">
                <a target="_blank" href="https://www.youtube.com/channel/UCcF2ZYgsn6rJ3lxCImMFYUA"><i class="fa fa-youtube"></i></a>
            </li>
        </ul>
    </div>
</div>
<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="{{assets_front()}}assets/images/logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.html">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url().'home/download_info' ?>">Info Pendaftaran</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url().'home/download_hasil' ?>">Lihat Hasil</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- END NAVBAR FIXED -->