<?php

class Panel extends CI_Controller
{
    protected $table = '';
    protected $subject = 'Panel';
    protected $module;

    function __construct()
    {
        parent::__construct();
        $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
        date_default_timezone_set('Asia/Jakarta');

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('panel/auth');
        }
//        var_dump($this->session->userdata());
//        exit();

    }

    public function index()
    {


        view_back("panel/views/v_dashboard", [], __DIR__);
    }
}