<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel_post extends CI_Controller {
        function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
                $this->load->library(array('form_validation', 'Recaptcha'));
        }
        
	public function index()
	{
        $data = array(
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
        );
        view("pages/home",$data,__DIR__);
        }

        public function add()
	{
                $data = array(
                        'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                        'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
                );
                if ($_POST) {
            
                        $this->form_validation->set_rules('nisn', 'nisn', 'required');
                        $recaptcha = $this->input->post('g-recaptcha-response');
                        $response = $this->recaptcha->verifyResponse($recaptcha);

                        if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true)
                        {
                                $data = [
                                        'status'    => 0,
                                        'message'   => validation_errors(),
                                ];
                        }

                        else {
                                $nama = $this->input->post('nama');
                                $nisn = $this->input->post('nisn');
                                $nik = $this->input->post('nik');
                                $asalSekolah = $this->input->post('asalSekolah');
                                $kotaLahir = $this->input->post('kotaLahir');
                                $tglLahir = $this->input->post('tglLahir');
                                $jenisKelamin = $this->input->post('jenisKelamin');
                                $agama = $this->input->post('agama');
                                $prodi = $this->input->post('prodi');

                                $config['upload_path']          = 'uploads/';
                                $config['allowed_types']        = 'zip|rar';

                                $this->load->library('upload', $config);

                                if ( ! $this->upload->do_upload('berkas'))
                                {
                                        $data = [
                                        'status'    => 0,
                                        'message'   => 'Upload Gagal! Karena '. $this->upload->display_errors(),
                                        ];
                                }
                                else {
                                        $data_upload = $this->upload->data();
                                        $data = array(
                                                'nisn' => $nisn,
                                                'nik' => $nik,
                                                'nama' => $nama,
                                                'asal_sekolah' => $asalSekolah,
                                                'kota_lahir' => $kotaLahir,
                                                'tgl_lahir' => $tglLahir,
                                                'jns_kelamin' => $jenisKelamin,
                                                'agama' => $agama,
                                                'prodi' => $prodi,
                                                'berkas' => $data_upload['file_name'],
                                        );
                                
                                        $this->db->insert('tb_pendaftaran',$data);
                                        $data = [
                                                'status'    => 1,
                                                'message'   => 'Data Berhasil Di Input'
                                        ];
                                }
                                header('Content-Type: application/json');
                                echo json_encode($data);
                                exit();
                        }
                        
                }
                        view("pages/home",$data,__DIR__);
        }

        
}
