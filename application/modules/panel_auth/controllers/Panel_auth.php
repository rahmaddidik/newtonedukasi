<?php
/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_Config $config
 * @property  CI_DB_query_builder $db
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 */
class Panel_Auth extends MX_Controller
{
    protected $table = '';
    protected $subject = 'Auth';
    protected $module;

    function __construct()
    {
        parent::__construct();
        $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
	{

        if ($this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('panel');
        }


            view_back( "panel_$this->module/views/v_login", [], __DIR__);

        
	}

	public  function login(){
        $this->form_validation->set_rules('email', str_replace(':', '',''), 'required');
        $this->form_validation->set_rules('password', str_replace(':', '',''), 'required');

        if ($this->form_validation->run() === TRUE)
        {
            $remember = (bool)$this->input->post('remember');
           if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
            {
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('panel');
            }else{
               $this->session->set_flashdata('message', 'Akun anda tidak ditemukan / tidak aktif');
               redirect('panel/auth');
           }

        }else{
            $this->session->set_flashdata('message', 'Validasi pengisian kurang tepat');
            redirect('panel/auth');
        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->ion_auth->logout();
        redirect('panel/auth');
    }
}
