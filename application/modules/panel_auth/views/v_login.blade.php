<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">

    <title>Login Dashboard Newtonedukasi</title>

    <link rel="apple-touch-icon" href="{{assets_back()}}base/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="{{assets_back()}}base/assets/images/favicon.ico">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{assets_back()}}global/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{assets_back()}}global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="{{assets_back()}}base/assets/css/site.min.css">
    <link rel="stylesheet" href="{{assets_back()}}base/assets/skins/red.min.css">

    <!-- Plugins -->
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/pages/login-v3.css">
    <link rel="stylesheet" href="{{assets_back()}}global/vendor/toastr/toastr.css">


    <!-- Fonts -->
    <link rel="stylesheet" href="{{assets_back()}}global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="{{assets_back()}}global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <style>
        .page-login-v3:before {
            background: #e6212a !important;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="{{assets_back()}}global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{assets_back()}}global/vendor/media-match/media.match.min.js"></script>
    <script src="{{assets_back()}}global/vendor/respond/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{assets_back()}}global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition page-login-v3 layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->



<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        <div class="panel">
            <div class="panel-body">
                <div class="brand">
                    <img class="brand-img" src="{{assets_back()}}base/assets/images/logo_color.png" alt="..."
                         style="width:150px">
                </div>
                {!!  form_open_multipart(base_url().'panel/auth/login',['method'=>'post']) !!}
                <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input type="email" class="form-control" name="email" required
                           oninvalid="this.setCustomValidity('Email tidak boleh kosong')"
                           oninput="setCustomValidity('')"/>
                    <label class="floating-label">Email</label>
                </div>
                <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input type="password" class="form-control" name="password" required
                           oninvalid="this.setCustomValidity('password tidak boleh kosong')"
                           oninput="setCustomValidity('')"/>
                    <label class="floating-label">Password</label>
                </div>
                <div class="form-group clearfix">
                    <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                        <input type="checkbox" id="inputCheckbox" name="remember">
                        <label for="inputCheckbox">Remember me</label>
                    </div>
                    <a class="float-right" href="forgot-password.html">Forgot password?</a>
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign in</button>
                {!!  form_close() !!}
            </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-twitter" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-facebook" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-google-plus" aria-hidden="true"></i>
                </a>
            </div>
        </footer>
    </div>
</div>
<!-- End Page -->


<!-- Core  -->
<script src="{{assets_back()}}global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
<script src="{{assets_back()}}global/vendor/jquery/jquery.js"></script>
<script src="{{assets_back()}}global/vendor/popper-js/umd/popper.min.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap/bootstrap.js"></script>
<script src="{{assets_back()}}global/vendor/animsition/animsition.js"></script>
<script src="{{assets_back()}}global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="{{assets_back()}}global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
<script src="{{assets_back()}}global/vendor/asscrollable/jquery-asScrollable.js"></script>
<script src="{{assets_back()}}global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

<!-- Plugins -->
<script src="{{assets_back()}}global/vendor/switchery/switchery.js"></script>
<script src="{{assets_back()}}global/vendor/intro-js/intro.js"></script>
<script src="{{assets_back()}}global/vendor/screenfull/screenfull.js"></script>
<script src="{{assets_back()}}global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="{{assets_back()}}global/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Scripts -->
<script src="{{assets_back()}}global/js/Component.js"></script>
<script src="{{assets_back()}}global/js/Plugin.js"></script>
<script src="{{assets_back()}}global/js/Base.js"></script>
<script src="{{assets_back()}}global/js/Config.js"></script>

<script src="{{assets_back()}}base/assets/js/Section/Menubar.js"></script>
<script src="{{assets_back()}}base/assets/js/Section/GridMenu.js"></script>
<script src="{{assets_back()}}base/assets/js/Section/Sidebar.js"></script>
<script src="{{assets_back()}}base/assets/js/Section/PageAside.js"></script>
<script src="{{assets_back()}}base/assets/js/Plugin/menu.js"></script>

<script src="{{assets_back()}}global/js/config/colors.js"></script>
<script src="{{assets_back()}}base/assets/js/config/tour.js"></script>
<script>Config.set('assets', '{{assets_back()}}base/assets');</script>

<!-- Page -->
<script src="{{assets_back()}}base/assets/js/Site.js"></script>
<script src="{{assets_back()}}global/js/Plugin/asscrollable.js"></script>
<script src="{{assets_back()}}global/js/Plugin/slidepanel.js"></script>
<script src="{{assets_back()}}global/js/Plugin/switchery.js"></script>
<script src="{{assets_back()}}global/js/Plugin/jquery-placeholder.js"></script>
<script src="{{assets_back()}}global/js/Plugin/material.js"></script>
<script src="{{assets_back()}}global/vendor/toastr/toastr.js"></script>

<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
            @php
                $CI = &get_instance();

            @endphp
            @if($CI->session->flashdata('message'))
                toastr['info']('{{$CI->session->flashdata('message')}}');
            @endif

        });
    })(document, window, jQuery);

</script>
</body>
</html>
