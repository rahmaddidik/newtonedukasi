@extends("app")

@section("head")
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/tables/datatable.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/toastr/toastr.css">
<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/uikit/modals.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
@endsection

@section("content")
<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Data Buku</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ base_url('panel') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Master Data</li>
            <li class="breadcrumb-item active">Data Buku</li>
        </ol>
    </div>

    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- Panel Basic -->
                <div class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <button data-target="#modAddBuku" data-toggle="modal" type="button" class="btn btn-block btn-primary"><i class="icon wb-plus"></i> Tambah Buku</button>
                            </div>
                            <h3 class="panel-title">Daftar Buku</h3>
                        </header>
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
                                <thead>
                                    <tr>
                                        <th>Kode Orin</th>
                                        <th>ISBN</th>
                                        <th>Kategori</th>
                                        <th>Judul</th>
                                        <th>Harga (Rp)</th>
                                        <th>Diskon</th>
                                        <th>AKSI</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Kode Orin</th>
                                        <th>ISBN</th>
                                        <th>Kategori</th>
                                        <th>Judul</th>
                                        <th>Harga (Rp)</th>
                                        <th>Diskon</th>
                                        <th>AKSI</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>A013</td>
                                        <td>978-602-8519-93-9</td>
                                        <td>CLASS</td>
                                        <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                        <td class="text-right">159.800</td>
                                        <td class="text-right">35%</td>
                                        <td class="text-right">
                                            <a data-target="#modEditBuku" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row" data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                            <a href="#" id="deleteSup" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End Panel Basic -->
            </div>
        </div>
    </div>
</div>
<!-- End Page -->

<!-- Modal Add Buku-->
<div class="modal fade" id="modAddBuku" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="exampleFormModalLabel">Tambah Buku</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">KODE PENERBIT : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="kode_penerbit" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ISBN : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="isbn" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">KATEGORI PAJAK :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="pajak" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">KATEGORI BUKU : </label>
                        <div class="col-md-9">
                            <select class="form-control" name="kategori_buku">
                                <option value="departement">Department</option>
                                <option value="class">Class</option>
                                <option value="subclass">Subclass</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">JUDUL : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="judul" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">DESKRIPSI : </label>
                        <div class="col-md-9">
                            <textarea class="form-control" rows="2" name="deskripsi"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PANJANG (CM) : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control text-right" name="p" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">LEBAR (CM) :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control text-right" name="l" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">TEBAL (MM) : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control text-right" name="t" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">BERAT NETTO : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control text-right" name="netto" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label text-right">BERAT BRUTO : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bruto" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">UOM : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="uom" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label text-right">HARGA :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="harga" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label text-right">DISKON KE GRAMEDIA (%): </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="diskon" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label text-right">NETTO DISTRIBUTOR : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="netto_distrib" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PENERBIT : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="penerbit" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PUBLISHED DATE : </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PUBLISHER INDICATOR : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="pub_indikator" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PENGARANG :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="pengarang" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">JUMLAH HALAMAN : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control text-right" name="jumlah_hal" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">SYNOPSIS : </label>
                        <div class="col-md-9">
                            <textarea rows="5" class="form-control" name="sinopsis"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">LAYOUTER-DESAINER : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="desainer" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PENERJEMAH : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="penerjemah" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">EDITOR :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="editor" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">PERCETAKAN : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="percetakan" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">BONUS : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bonus" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">TAG : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tag" placeholder="">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">KODE ORIN : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="kode_orin" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">JENIS KERTAS : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jenis_kertas" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">COVER (PNG)</label>
                        <div class="col-md-9">
                            <div class="input-group input-group-file" data-plugin="inputGroupFile">
                                <input type="text" class="form-control" readonly="">
                                <span class="input-group-btn">
                                <span class="btn btn-outline btn-file">
                                    <i class="icon wb-upload" aria-hidden="true"></i>
                                    <input type="file" name="" multiple="">
                                </span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add Buku-->

<!-- Modal Edit Buku-->
<div class="modal fade" id="modEditBuku" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="exampleFormModalLabel">Edit Buku</h4>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Buku-->

@endsection

    
@section("footer")
<!-- Plugins -->
<script src="{{assets_back()}}global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>

<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="{{assets_back()}}global/vendor/toastr/toastr.js"></script>

<script src="{{assets_back()}}global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Page -->
<script src="{{assets_back()}}global/js/Plugin/datatables.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/tables/datatable.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootbox.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/toastr.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/jquery-placeholder.js"></script>
<script src="{{assets_back()}}global/js/Plugin/input-group-file.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-datepicker.js"></script>
@endsection