@extends("app")

@section("head")
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/uikit/modals.css">
@endsection

@section("content")

<!-- Page -->
<div class="page">
  <div class="page-header">
    <h1 class="page-title">Data Diskon</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ base_url('panel') }}">Dashboard</a>
      </li>
      <li class="breadcrumb-item">Master Data</li>
      <li class="breadcrumb-item active">Data Diskon</li>
    </ol>
  </div>

  <div class="page-content">
    <div class="row">
      <div class="col-md-6">
        <!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            <div class="panel-actions">
              <button data-target="#modAddDiskon" data-toggle="modal" type="button" class="btn btn-block btn-primary">
                <i class="icon wb-plus"></i> Tambah Diskon</button>
            </div>
            <h3 class="panel-title">Diskon Suplier</h3>
          </header>
          <div class="panel-body">
            <table id="tb_diskon_suplier" class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>Kode Diskon</th>
                  <th>DTP</th>
                  <th>BKP</th>
                  <th>AlQURAN</th>
                  <th>KHUSUS</th>
                  <th>AKSI</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Kode Diskon</th>
                  <th>DTP</th>
                  <th>BKP</th>
                  <th>AlQURAN</th>
                  <th>KHUSUS</th>
                  <th>AKSI</th>
                </tr>
              </tfoot>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
      <div class="col-md-6">
        <!-- Panel Basic -->
        <div class="panel">
          <header class="panel-heading">
            <div class="panel-actions">
              <button data-target="#modAddDiskon" data-toggle="modal" type="button" class="btn btn-block btn-primary">
                <i class="icon wb-plus"></i> Tambah Diskon</button>
            </div>
            <h3 class="panel-title">Diskon Relasi</h3>
          </header>
          <div class="panel-body">
            <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
              <thead>
                <tr>
                  <th>Kode Diskon</th>
                  <th>DTP</th>
                  <th>BKP</th>
                  <th>AlQURAN</th>
                  <th>KHUSUS</th>
                  <th>AKSI</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Kode Diskon</th>
                  <th>DTP</th>
                  <th>BKP</th>
                  <th>AlQURAN</th>
                  <th>KHUSUS</th>
                  <th>AKSI</th>
                </tr>
              </tfoot>
              <tbody id="data_table">
                <tr>
                  <td>Kode Diskon</td>
                  <td>DTP</td>
                  <td>BKP</td>
                  <td>ALQURAN</td>
                  <td>KHUSUS</td>
                  <td>
                    <a data-target="#modEditDiskon" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                      data-toggle="tooltip" data-original-title="Edit">
                      <i class="icon wb-edit" aria-hidden="true"></i>
                    </a>
                    <a href="#" id="deleteSup" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip" data-original-title="Remove">
                      <i class="icon wb-trash" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
    </div>
  </div>
</div>
<!-- End Page -->

<!-- Modal Add Diskon-->
<div class="modal fade" id="modAddDiskon" aria-hidden="false" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="exampleFormModalLabel">Tambah Diskon
          <small>Suplier</small>
        </h4>
      </div>
      <div class="modal-body">
        <form id="form-add" class="form-horizontal" method="post" enctype="multipart/form-data"
              onsubmit="return false;">
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Kode Diskon : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="kode_diskon" placeholder=""  required oninvalid="this.setCustomValidity('Kode diskon tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">DTP : </label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="dtp" placeholder="" required oninvalid="this.setCustomValidity('Diskon DTP tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">BKP :</label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="bkp" placeholder="" required oninvalid="this.setCustomValidity('Diskon BKP tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">ALQURAN : </label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="alquran" placeholder="" required oninvalid="this.setCustomValidity('Diskon Al-qur an tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">KHUSUS : </label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="khusus" placeholder="" required oninvalid="this.setCustomValidity('Diskon Khusus tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group text-right row">
            <div class="col-md-9 offset-md-3">
              <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
              <button class="btn btn-primary" type="submit">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Add Diskon-->

<!-- Modal Edit Diskon-->
<div class="modal fade" id="modEditDiskon" aria-hidden="false" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="exampleFormModalLabel">Edit Diskon
          <small>Suplier</small>
        </h4>
      </div>
      <div class="modal-body">
        <form id="form-edit" class="form-horizontal" method="post" enctype="multipart/form-data"
              onsubmit="return false;">
          <div class="form-group row">
            <label class="col-md-3 form-control-label">Kode Diskon : </label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="kode_diskon2" required oninvalid="this.setCustomValidity('Kode diskon tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">DTP : </label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="dtp2" required oninvalid="this.setCustomValidity('Diskon DTP tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">BKP :</label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="bkp2" required oninvalid="this.setCustomValidity('Diskon BKP tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">ALQURAN : </label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="alquran2" required oninvalid="this.setCustomValidity('Diskon Al-qur an tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 form-control-label">KHUSUS : </label>
            <div class="col-md-9">
              <input type="number" class="form-control" id="khusus2" required oninvalid="this.setCustomValidity('Diskon Khusus tidak boleh kosong') " oninput="setCustomValidity('')">
            </div>
          </div>
          <div class="form-group text-right row">
            <div class="col-md-9 offset-md-3">
              <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
              <button class="btn btn-primary" type="submit">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Edit Diskon-->

@endsection

    
@section("footer")
<!-- Plugins -->
<script src="{{assets_back()}}global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>

<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="{{assets_back()}}global/vendor/toastr/toastr.js"></script>

<!-- Page -->
<script src="{{assets_back()}}global/js/Plugin/datatables.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/tables/datatable.js"></script>

<script src="{{assets_back()}}global/js/Plugin/bootbox.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/toastr.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>

<script>
    var table;


    // *onready
    $(document).ready(function () {
        refresh_table();
    });

    // *refresh table
    function refresh_table() {

        $.get('{{base_url()}}panel/data_diskon/get', function (data) {

            if (table) table.clear();

            table = $('#tb_diskon_suplier').DataTable({
                destroy: true,
                paging: true,
                searching: true
            });

            $.each(data['data'], function (index, value) {
                table.row.add([
                    value['kode_diskon'],
                    value['dtp']+' %',
                    value['bkp']+' %',
                    value['alquran']+' %',
                    value['khusus']+' %',
                    '<a onclick="edit(' + value['id_diskon'] + ')" data-target="#modEditDiskon" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"  data_id="' + value['id_diskon'] + '" data-toggle="tooltip" data-original-title="Edit">\n' +
                    '<i class="icon wb-edit" aria-hidden="true"></i> </a>' + '' +
                    '<a onclick="confirmDelete(' + value['id_diskon'] + ')" data-toggle="tooltip" data-original-title="Remove" class="btn btn-sm btn-icon btn-pure btn-default">' +
                    '<i class="icon wb-trash" aria-hidden="true"></i></a>'
                ]).draw();
            });
        });
    }


    // *create
    $('#form-add').submit(function () {

        $.post('{{base_url()}}panel/data_diskon/add', {
                kode_diskon: $('#kode_diskon').val(),
                dtp: $('#dtp').val(),
                bkp: $('#bkp').val(),
                alquran: $('#alquran').val(),
                khusus: $('#khusus').val()
            },
            function (data) {

                refresh_table();

                if (data.status == 1) {
                    swal({
                            title: "Berhasil!",
                            text: "Data telah ditambahkan!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            clear_input('form-add');
                        });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            }).fail(function () {
            swal("Gagal!", "Kode Kategori sudah ada", "error");
        });
    });

    // *edit
    function edit(id) {
        $.get('{{base_url()}}panel/data_diskon/edit/' + id,
            function (data) {

                refresh_table();

                if (data.status == 1) {
                    $('#kode_diskon2').val(data['data'][0]['kode_diskon']);
                    $('#dtp2').val(data['data'][0]['dtp']);
                    $('#bkp2').val(data['data'][0]['bkp']);
                    $('#alquran2').val(data['data'][0]['alquran']);
                    $('#khusus2').val(data['data'][0]['khusus']);
                    id_update = id;
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
    }

    // #update
    $('#form-edit').submit(function () {

        $.post('{{base_url()}}panel/data_diskon/update/' + id_update, {
                kode_diskon: $('#kode_diskon2').val(),
                dtp: $('#dtp2').val(),
                bkp: $('#bkp2').val(),
                alquran: $('#alquran2').val(),
                khusus: $('#khusus2').val()
            },
            function (data) {

                refresh_table();

                if (data.status == 1) {

                    swal({
                            title: "Berhasil!",
                            text: "Data telah di update!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            $('#modEditDiskon').modal('hide');
                        });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
    });

    // *delete
    function confirmDelete(id) {

        swal({
            title: "Data Kategori Buku",
            text: "Apa anda yakin ingin menghapus data ini?",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data",
            cancelButtonText: "Batalkan",
            closeOnConfirm: false
        }, function () {

            $.post('{{base_url()}}panel/data_diskon/delete/' + id, function (data) {

                if (data.status === 1) {

                    refresh_table();

                    swal({
                            title: "Berhasil!",
                            text: "Data telah dihapus!",
                            type: "success",
                            showCloseButton: true,
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            clear_input('form-add');
                        });
                }
                else {
                    refresh_table();
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
        });
    }
</script>

@endsection