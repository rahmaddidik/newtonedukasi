<?php

class Panel_data_pemasok extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('panel/auth');
        }
    }

    public function index()
    {
        view_back( "panel_$this->module/views/v_data_pemasok", [], __DIR__);
    }


//    get data
    public function get()
    {
        $this->db->join('tb_diskon','tb_diskon.kode_diskon=tb_pemasok.kode_diskon');
        $penerbit_buku = $this->db->get('tb_pemasok');
        $data_diskon = $this->db->get('tb_diskon');
        if (!$this->input->is_ajax_request()) {

            $data = [
                'status' => 0,
                'message' => validation_errors(),
            ];
            exit('No direct script access allowed');
        } else {
            if ($this->input->server('REQUEST_METHOD') == 'GET') {
                $data = [
                    'status' => 1,
                    'message' => 'Data Berhasil Di Tampilkan',
                    'data' => $penerbit_buku->result_array(),
                    'data_2' => $data_diskon->result_array(),
                ];
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    add data
    public function add()
    {
        if (!$this->input->is_ajax_request()) {

            exit('No direct script access allowed');

        } else {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $insert_berhasil = $this->db->insert('tb_pemasok', $this->input->post());
                if ($insert_berhasil) {
                    $data = [
                        'status' => 1,
                        'message' => 'Data Berhasil Di Input'
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'message' => validation_errors(),
                    ];
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    edit data
    public function edit($id)
    {

        if (!$this->input->is_ajax_request()) {
            $data = [
                'status' => 0,
                'message' => validation_errors(),
            ];
            exit('No direct script access allowed');
        } else {
            $pencarian_berhasil = $this->db->get_where('tb_pemasok', array('id_penerbit' => $id));
            if ($this->input->server('REQUEST_METHOD') == 'GET') {
                $data = [
                    'status' => 1,
                    'message' => 'Data Berhasil Di Tampilkan',
                    'data' => $pencarian_berhasil->result_array()
                ];
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    update data
    public function update($id)
    {
        if (!$this->input->is_ajax_request()) {

            exit('No direct script access allowed');

        } else {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $update_berhasil = $this->db->update('tb_pemasok', $this->input->post(), array('id_penerbit' => $id));
                if ($update_berhasil) {
                    $data = [
                        'status' => 1,
                        'message' => 'Data Berhasil Di Update'
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'message' => validation_errors(),
                    ];
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    delete data
    public function delete($id)
    {

        if (!$this->input->is_ajax_request() and false) {
            $data = [
                'status' => 0,
                'message' => validation_errors(),
            ];
            exit('No direct script access allowed');
        } else {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $delete_berasil = $this->db->delete('tb_pemasok', array('id_penerbit' => $id));
                if ($delete_berasil) {
                    $data = [
                        'status' => 1,
                        'message' => 'Data Berhasil Di Delete'
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'message' => validation_errors(),
                    ];
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

}