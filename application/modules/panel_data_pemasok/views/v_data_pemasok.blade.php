@extends("app")

@section("head")
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/uikit/modals.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
@endsection

@section("content")

<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Data Suplier/Pemasok/Penerbit</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ base_url('panel') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Master Data</li>
            <li class="breadcrumb-item active">Data Pemasok</li>
        </ol>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button data-target="#modAddPenerbit" data-toggle="modal" type="button" class="btn btn-block btn-primary">
                        <i class="icon wb-plus"></i> Tambah Penerbit</button>
                </div>
                <h3 class="panel-title">Daftar Penerbit</h3>
            </header>
            <div class="panel-body">
                <table id="tb_pemasok_buku" class="table table-hover dataTable table-striped w-full">
                    <thead>
                        <tr>
                            <th rowspan="2">Nama Penerbit</th>
                            <th rowspan="2">No Hp</th>
                            <th rowspan="2">Email</th>
                            <th rowspan="2">Tgl Masuk</th>
                            <th colspan="4" class="text-center">Diskon</th>
                            <th rowspan="2">Aksi</th>
                        </tr>
                        <tr>
                            <th>DTP</th>
                            <th>BKP</th>
                            <th>ALQURAN</th>
                            <th>KHUSUS</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nama Penerbit</th>
                            <th>No Hp</th>
                            <th>Email</th>
                            <th>Tgl Masuk</th>
                            <th>20%</th>
                            <th>18%</th>
                            <th>35%</th>
                            <th>40%</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Panel Basic -->

    </div>
</div>
<!-- End Page -->

<!-- Modal Add Penerbit-->
<div class="modal fade" id="modAddPenerbit" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Tambah Penerbit</h4>
            </div>
            <div class="modal-body">
                <form id="form-add" class="form-horizontal" method="post" enctype="multipart/form-data"
                      onsubmit="return false;">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="kode_pemasok" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Nama Penerbit : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="nama_penerbit" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Hp :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="no_hp" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Email : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="email" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Rekening :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="rekening" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Tgl Masuk : </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" placeholder="" id="tgl_masuk" data-date-format="yyyy/mm/dd">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Diskon :</label>
                        <div class="col-md-9">
                            <select  class="form-control" id="kode_diskon">

                            </select>
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="submit">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add Penerbit-->

<!-- Modal Edit Penerbit-->
<div class="modal fade" id="modEditPenerbit" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Edit Penerbit</h4>
            </div>
            <div class="modal-body">
                <form id="form-edit" class="form-horizontal" method="post" enctype="multipart/form-data"
                      onsubmit="return false;">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="kode_pemasok2" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Nama Penerbit : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  id="nama_penerbit2" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">No Hp :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="no_hp2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Email : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="email2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Rekening :</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="rekening2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Tgl Masuk : </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control" data-plugin="datepicker" id="tgl_masuk2" data-date-format="yyyy/mm/dd">
                                <span class="input-group-addon">
                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Kode Diskon :</label>
                        <div class="col-md-9">
                            <select  class="form-control" id="kode_diskon2">

                            </select>
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Penerbit-->

@endsection

    
@section("footer")
<!-- Plugins -->
<script src="{{assets_back()}}global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>

<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="{{assets_back()}}global/vendor/toastr/toastr.js"></script>

<script src="{{assets_back()}}global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Page -->
<script src="{{assets_back()}}global/js/Plugin/datatables.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/tables/datatable.js"></script>

<script src="{{assets_back()}}global/js/Plugin/bootbox.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/toastr.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/jquery-placeholder.js"></script>
<script src="{{assets_back()}}global/js/Plugin/input-group-file.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-datepicker.js"></script>


<script>
    var table;


    // *onready
    $(document).ready(function () {
        refresh_table();
    });

    // *refresh table
    function refresh_table() {

        $.get('{{base_url()}}panel/data_pemasok/get', function (data) {
            data_diskon='';
            $.each(data['data_2'],function (index,value){
                if(index==0){
                    data_diskon=data_diskon+'<option value="'+value['kode_diskon']+'" selected>'+value['kode_diskon']+'</option>';
                }else{
                    data_diskon=data_diskon+'<option value="'+value['kode_diskon']+'">'+value['kode_diskon']+'</option>';
                }

            });
            $('#kode_diskon').html(data_diskon);
            $('#kode_diskon2').html(data_diskon);
            if (table) table.clear();
            table = $('#tb_pemasok_buku').DataTable({
                destroy: true,
                paging: true,
                searching: true
            });

            $.each(data['data'], function (index, value) {

                table.row.add([
                    value['nama_penerbit'],
                    value['no_hp'],
                    value['email'],
                    value['tgl_masuk'],
                    value['dtp']+' %',
                    value['bkp']+' %',
                    value['alquran']+' %',
                    value['khusus']+' %',
                    '<a onclick="edit(' + value['id_penerbit'] + ')" data-target="#modEditPenerbit" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"  data_id="' + value['id_penerbit'] + '" data-toggle="tooltip" data-original-title="Edit">\n' +
                    '<i class="icon wb-edit" aria-hidden="true"></i> </a>' + '' +
                    '<a onclick="confirmDelete(' + value['id_penerbit'] + ')" data-toggle="tooltip" data-original-title="Remove" class="btn btn-sm btn-icon btn-pure btn-default">' +
                    '<i class="icon wb-trash" aria-hidden="true"></i></a>'
                ]).draw();

            });
        });
    }


    // *create
    $('#form-add').submit(function () {

        $.post('{{base_url()}}panel/data_pemasok/add', {
                kode_pemasok: $('#kode_pemasok').val(),
                nama_penerbit: $('#nama_penerbit').val(),
                no_hp: $('#no_hp').val(),
                email: $('#email').val(),
                rekening: $('#rekening').val(),
                tgl_masuk: $('#tgl_masuk').val(),
                kode_diskon: $('#kode_diskon').val(),
            },
            function (data) {

                refresh_table();

                if (data.status == 1) {
                    swal({
                            title: "Berhasil!",
                            text: "Data telah ditambahkan!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            clear_input('form-add');
                        });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            }).fail(function () {
            swal("Gagal!", "Kode Kategori sudah ada", "error");
        });
    });

    // *edit
    function edit(id) {
        $.get('{{base_url()}}panel/data_pemasok/edit/' + id,
            function (data) {


                if (data.status == 1) {
                    $('#kode_pemasok2').val(data['data'][0]['kode_pemasok']);
                    $('#nama_penerbit2').val(data['data'][0]['nama_penerbit']);
                    $('#no_hp2').val(data['data'][0]['no_hp']);
                    $('#email2').val(data['data'][0]['email']);
                    $('#rekening2').val(data['data'][0]['rekening']);
                    $('#tgl_masuk2').val(data['data'][0]['tgl_masuk']);
                    $('#kode_diskon2').val(data['data'][0]['kode_diskon']);
                    id_update = id;
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });

    }

    // #update
    $('#form-edit').submit(function () {

        $.post('{{base_url()}}panel/data_pemasok/update/' + id_update, {
                kode_pemasok: $('#kode_pemasok2').val(),
                nama_penerbit: $('#nama_penerbit2').val(),
                no_hp: $('#no_hp2').val(),
                email: $('#email2').val(),
                rekening: $('#rekening2').val(),
                tgl_masuk: $('#tgl_masuk2').val(),
                kode_diskon: $('#kode_diskon2').val(),
            },
            function (data) {

                refresh_table();

                if (data.status == 1) {

                    swal({
                            title: "Berhasil!",
                            text: "Data telah di update!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            $('#modEditPenerbit').modal('hide');
                        });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
    });

    // *delete
    function confirmDelete(id) {

        swal({
            title: "Data Kategori Buku",
            text: "Apa anda yakin ingin menghapus data ini?",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus Data",
            cancelButtonText: "Batalkan",
            closeOnConfirm: false
        }, function () {

            $.post('{{base_url()}}panel/data_pemasok/delete/' + id, function (data) {

                if (data.status === 1) {

                    refresh_table();

                    swal({
                            title: "Berhasil!",
                            text: "Data telah dihapus!",
                            type: "success",
                            showCloseButton: true,
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },
                        function () {
                            clear_input('form-add');
                        });
                }
                else {
                    refresh_table();
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }

            });
        });
    }
</script>

@endsection