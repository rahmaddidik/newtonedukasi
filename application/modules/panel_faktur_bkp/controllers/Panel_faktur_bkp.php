<?php

class Panel_faktur_bkp extends CI_Controller
{
    protected $table = '';
    protected $subject = 'Faktur BKP';
    protected $module;

    function __construct()
    {
        parent::__construct();
        $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('panel/auth');
        }
    }

    public function index()
    {    
        // $this->auth->check();
        view_back( "panel_$this->module/views/v_faktur_bkp", [], __DIR__);
    }

    public function cetak()
    {    
        // $this->auth->check();
        view_back( "panel_$this->module/views/v_faktur_bkp_cetak", [], __DIR__);
    }
}