@extends("app")

@section("head")
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/tables/datatable.css">

<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/toastr/toastr.css">

<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/uikit/modals.css">
<link rel="stylesheet" href="{{assets_back()}}global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="{{assets_back()}}base/assets/examples/css/pages/invoice.css">
@endsection

@section("content")

<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Faktur Keluar BKP Gramedia</h1>
    </div>

    <div class="page-content">
        <!-- Panel -->
        <div class="panel">
            <form class="form-horizontal" action="{{base_url()}}panel/faktur_bkp/cetak">
                <div class="panel-body container-fluid">
                    <div class="row">
                        <div class="col-lg-4">
                            <h3>
                                <img class="mr-10 mb-10" style="height: 50px" src="{{assets_back()}}base/assets/images/logo_color.png" alt="...">
                            </h3>
                            <address>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Relasi : </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="relasi" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Alamat : </label>
                                    <div class="col-md-9">
                                        <textarea rows="3" class="form-control" name="alamat" placeholder="" disabled></textarea>
                                    </div>
                                </div>
                            </address>
                        </div>
                        <div class="col-lg-4 offset-lg-4 text-right">
                            <h4>Info Faktur</h4>
                            <br>
                            <br>
                            <address>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Kode Relasi : </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="kode_relasi">
                                            <option value="" selected hidden>Pilih</option>
                                            <option value="KR005">KR005</option>
                                            <option value="KR006">KR006</option>
                                            <option value="KR007">KR007</option>
                                            <option value="KR008">KR008</option>
                                            <option value="KR009">KR009</option>
                                            <option value="KR010">KR010</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Tgl Faktur : </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-plugin="datepicker" placeholder="">
                                            <span class="input-group-addon">
                                                <i class="icon wb-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">No Faktur : </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="no_faktur" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Pajak : </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="pajak" placeholder="" disabled>
                                    </div>
                                </div>
                            </address>
                        </div>
                    </div>

                    <div class="row mb-20">
                        <h3 class="panel-title">Daftar Pemesanan</h3>
                        <div class="mt-20" style="position:absolute; right:30px;">
                            <button data-target="#modAddFaktur" data-toggle="modal" type="button" class="btn btn-block btn-primary">
                                <i class="icon wb-plus"></i> Tambah</button>
                        </div>
                    </div>

                    <div class="page-invoice-table table-responsive mt-0">
                        <table class="table table-hover dataTable table-striped w-full" id="exampleFixedHeader">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Orin</th>
                                    <th>Judul Buku</th>
                                    <th>Harga Jual</th>
                                    <th>Diskon</th>
                                    <th>Jumlah</th>
                                    <th>Nilai Jual</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>A025</td>
                                    <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                    <td>Rp159.800</td>
                                    <td>35%</td>
                                    <td>100</td>
                                    <td>Rp11.980.000</td>
                                    <td>-</td>
                                    <td>
                                        <a data-target="#modEditFaktur" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                            data-toggle="tooltip" data-original-title="Edit">
                                            <i class="icon wb-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" id="exampleWarningConfirm" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip"
                                            data-original-title="Remove">
                                            <i class="icon wb-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>A025</td>
                                    <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                    <td>Rp159.800</td>
                                    <td>35%</td>
                                    <td>100</td>
                                    <td>Rp11.980.000</td>
                                    <td>-</td>
                                    <td>
                                        <a data-target="#modEditFaktur" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                            data-toggle="tooltip" data-original-title="Edit">
                                            <i class="icon wb-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" id="exampleWarningConfirm" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip"
                                            data-original-title="Remove">
                                            <i class="icon wb-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>A025</td>
                                    <td>THE KING BEDAH KISI-KISI UN SMA IPS 2019</td>
                                    <td>Rp159.800</td>
                                    <td>35%</td>
                                    <td>100</td>
                                    <td>Rp11.980.000</td>
                                    <td>-</td>
                                    <td>
                                        <a data-target="#modEditFaktur" data-toggle="modal" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                            data-toggle="tooltip" data-original-title="Edit">
                                            <i class="icon wb-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" id="exampleWarningConfirm" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row" data-toggle="tooltip"
                                            data-original-title="Remove">
                                            <i class="icon wb-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="text-right clearfix">
                        <div class="float-right">
                            <p>Bruto:
                                <span>Rp33.598.000</span>
                            </p>
                            <p>Diskon:
                                <span>Rp2.500.000</span>
                            </p>
                            <p class="page-invoice-amount">Netto:
                                <span>Rp30.098.000</span>
                            </p>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-animate btn-animate-side btn-primary">
                            <span>
                                <i class="icon wb-check" aria-hidden="true"></i> Simpan Faktur</span>
                        </button>
                        <button type="button" class="btn btn-animate btn-animate-side btn-default btn-outline" onclick="javascript();">
                            <span>
                                <i class="icon wb-print" aria-hidden="true"></i> Cetak</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Panel -->
    </div>
</div>
<!-- End Page -->

<!-- Modal Add Retur-->
<div class="modal fade" id="modAddFaktur" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Tambah Faktur BKP</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ID ORIN: </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="id_orin" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Jumlah : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jumlah" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add Retur-->

<!-- Modal Edit Retur-->
<div class="modal fade" id="modEditFaktur" aria-hidden="false" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleFormModalLabel">Edit Retur Masuk</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">ID ORIN: </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="id_orin" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-control-label">Jumlah : </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="jumlah" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-right row">
                        <div class="col-md-9 offset-md-3">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="button" id="swalAddSuccess">Tambahkan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Retur-->

@endsection

    
@section("footer")
<!-- Plugins -->
<script src="{{assets_back()}}global/vendor/datatables.net/jquery.dataTables.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
<script src="{{assets_back()}}global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>

<script src="{{assets_back()}}global/vendor/bootbox/bootbox.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-sweetalert/sweetalert.js"></script>
<script src="{{assets_back()}}global/vendor/toastr/toastr.js"></script>

<script src="{{assets_back()}}global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="{{assets_back()}}global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Page -->
<script src="{{assets_back()}}global/js/Plugin/datatables.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/tables/datatable.js"></script>

<script src="{{assets_back()}}global/js/Plugin/bootbox.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/toastr.js"></script>
<script src="{{assets_back()}}base/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
<script src="{{assets_back()}}global/js/Plugin/jquery-placeholder.js"></script>
<script src="{{assets_back()}}global/js/Plugin/input-group-file.js"></script>
<script src="{{assets_back()}}global/js/Plugin/bootstrap-datepicker.js"></script>

@endsection