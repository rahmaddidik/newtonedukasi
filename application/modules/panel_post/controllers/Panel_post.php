<?php

class Panel_post extends CI_Controller
{
    protected $table = '';
    protected $subject = 'Post';
    protected $module;

    function __construct()
    {
        parent::__construct();
        $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('panel/auth');
        }
    }

    public function index()
    {       
        $this->auth->check();
        view_back( "panel_$this->module/views/v_post", [], __DIR__);
    }

    public function add()
	{
                if ($_POST) {
            
                        $this->form_validation->set_rules('judul', 'judul', 'required');
                        $this->form_validation->set_rules('status', 'status', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                                $data = [
                                        'status'    => 0,
                                        'message'   => validation_errors(),
                                ];
                        }

                        else {
                                $judul = $this->input->post('judul');
                                $status = $this->input->post('status');

                                $config['upload_path']          = 'uploads/post/';
                                $config['allowed_types']        = 'jpg|jpeg|png|rar|zip|pdf|xlsx|xls';
                                $config['max_size']             = '2000';

                                $this->load->library('upload', $config);

                                if ( ! $this->upload->do_upload('dokumen'))
                                {
                                        $data = [
                                        'status'    => 0,
                                        'message'   => 'Upload Gagal! Karena '. $this->upload->display_errors(),
                                        ];
                                }
                                else {
                                        $data_upload = $this->upload->data();
                                        $data = array(
                                                'judul' => $judul,
                                                'status' => $status,
                                                'dokumen' => $data_upload['file_name'],
                                        );
                                
                                        $this->db->insert('tb_post',$data);
                                        $data = [
                                                'status'    => 1,
                                                'message'   => 'Data Berhasil Di Input'
                                        ];
                                }
                                header('Content-Type: application/json');
                                echo json_encode($data);
                                exit();
                        }
                        
                }
                view_back( "panel_$this->module/views/v_post", [], __DIR__);
        }

}