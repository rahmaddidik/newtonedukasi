@extends("app")
@section("content")

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <!-- Example DataTables Card-->
                <div class="card mb-3">
                    <div class="card-header">
                    <i class="fa fa-pencil-square-o"></i> Post Info Pendaftaran</div>
                    <div class="card-body">
                        <form id="form-add-info" action="<?php echo base_url()?>panel_post/add" method="post" enctype="multipart/form-data" onsubmit="return false;">
                            <input type="text" name="status" value="1" hidden required="">
                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text" class="form-control" id="judul" name="judul" placeholder="">
                            </div>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="dokumen" id="inputGroupFile01"/>
                                    <label class="custom-file-label" for="inputGroupFile01"><i>Pilih berkas</i></label>
                                </div>
                            </div>
                            <div class="form-group mt-3 mb-0 text-right">
                                <button type="submit" class="btn btn-primary btn-lg">Unggah Info Pendaftaran</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <!-- Example DataTables Card-->
                <div class="card mb-3">
                    <div class="card-header">
                    <i class="fa fa-pencil-square-o"></i> Post Hasil Kelulusan</div>
                    <div class="card-body">
                        <form id="form-add-hasil" action="<?php echo base_url()?>panel_post/add" method="post" enctype="multipart/form-data" onsubmit="return false;">
                            <input type="text" name="status" value="0" hidden required="">
                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text" class="form-control" id="judul" name="judul" placeholder="">
                            </div>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="dokumen" id="inputGroupFile01"/>
                                    <label class="custom-file-label" for="inputGroupFile01"><i>Pilih berkas</i></label>
                                </div>
                            </div>
                            <div class="form-group mt-3 mb-0 text-right">
                                <button type="submit" class="btn btn-primary btn-lg">Unggah Hasil Kelulusan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- /.container-fluid-->
</div>

@endsection

@section("footer")

<script>
$('.custom-file-input').on('change', function() { 
   let fileName = $(this).val().split('\\').pop(); 
   $(this).next('.custom-file-label').addClass("selected").html(fileName); 
});
</script>

<script>
// this is the id of the form ajax
$("#form-add-info").submit(function () {
  $(".loader").addClass("active");
      var formData = new FormData($(this)[0]);
      var form = $(this);
      $.ajax({
          url: form.attr('action'), 
          type: "POST",
          data: formData,
          cache: false,
          contentType: false,
          async: false,
          processData: false,
          success: function (data) {
              console.log(data);
              test = data;
              $(".loader").removeClass("active");
              if (data.status == 1) {
                  swal("Terima Kasih!", "Anda Berhasil Mendaftar", "success").then((value) => {
                  switch (value) {
                    default:
                    location.reload();
                    break;
                  }
                });
              }
              else {
                swal("Gagal!", "Coba Periksa Lagi", "error");
              }
          }
      });
});

$("#form-add-hasil").submit(function () {
  $(".loader").addClass("active");
      var formData = new FormData($(this)[0]);
      var form = $(this);
      $.ajax({
          url: form.attr('action'), 
          type: "POST",
          data: formData,
          cache: false,
          contentType: false,
          async: false,
          processData: false,
          success: function (data) {
              console.log(data);
              test = data;
              $(".loader").removeClass("active");
              if (data.status == 1) {
                  swal("Terima Kasih!", "Anda Berhasil Mendaftar", "success").then((value) => {
                  switch (value) {
                    default:
                    location.reload();
                    break;
                  }
                });
              }
              else {
                swal("Gagal!", "Coba Periksa Lagi", "error");
              }
          }
      });
});

</script>

@endsection