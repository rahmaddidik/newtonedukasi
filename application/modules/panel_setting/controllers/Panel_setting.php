<?php

class Panel_setting extends CI_Controller {
        function __construct()
        {
                parent::__construct();
                $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
                $this->load->helper(array('form', 'url'));
                $this->load->library('form_validation');
                date_default_timezone_set('Asia/Jakarta');
            if (!$this->ion_auth->logged_in())
            {
                // redirect them to the login page
                redirect('panel/auth');
            }
        }
        
	public function index()
	{
                $this->auth->check();
                $admin = $this->db->get('tb_admin');
                $data = array(
                'admin' => $admin->result_array() 
                );

                view_back( "panel_$this->module/views/v_setting", $data, __DIR__);
        }
        
        public function update(){
                if ($_POST) {
                        $this->form_validation->set_rules('nama', 'nama', 'required');
                        $this->form_validation->set_rules('email', 'email', 'required');

                        if ($this->form_validation->run() == FALSE)
                        {
                                $data = [
                                        'status'    => 0,
                                        'message'   => validation_errors(),
                                ];
                        }
                        else {
                                $id_admin = $this->input->post('id_admin');
                                $email = $this->input->post('email');
                                $password = $this->input->post('password');
                                $nama = $this->input->post('nama');
                                // var_dump($id_admin, $email, $password, $nama);

                                $data = array(
                                        'email' => $email,
                                        'password' => sha1($password),
                                        'nama' => $nama,
                                );

                                $this->db->update('tb_admin', $data, 'id_admin='.$id_admin);
                                $data = [
                                        'status'    => 1,
                                        'message'   => 'Data Berhasil Di Input'
                                ];
                        }
                        header('Content-Type: application/json');
                        echo json_encode($data);
                        exit();
                }
                view_back( "panel_$this->module/views/v_setting", [], __DIR__);
        }
}
