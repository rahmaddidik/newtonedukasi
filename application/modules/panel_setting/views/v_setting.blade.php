@extends("app")

@section("head")
@endsection

@section("content")

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-sm-5">
                <!-- Example DataTables Card-->
                <div class="card card-setting mb-3">
                    <div class="card-header">
                    <h3>Edit Administrator</h3>
                    </div>
                    <div class="card-body">
                        <form id="form-update" action="<?php echo base_url()?>panel_setting/update" method="post" enctype="multipart/form-data" onsubmit="return false;"> 
                            <?php foreach ($admin as $v): ?>
                                <input type="text" class="d-none" value="<?php echo $v['id_admin']?>" name="id_admin">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="nama" class="form-control" value="<?php echo $v['nama']?>" id="nama" name="nama" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" value="<?php echo $v['email']?>" id="email" name="email" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="">
                                    <div class="switch-pass" id="switchPass" onclick="myFunction()">Lihat</div>
                                </div>
                                <div class="form-group mt-3 mb-0 text-right">
                                    <button type="submit" class="btn btn-primary btn-lg">Edit Admin</button>
                                </div>
                            <?php endforeach; ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
</div>

@endsection

@section('footer')
<script>
    $(document).ready(function(){
        $('.open').click(function(){
        
            $('.showpanel').slideToggle('slow');
            if($(this).text() == 'close')
            {
                $(this).text('Show');
            }
            else
            {
                $(this).text('close');
            }
        });
    });
    
    function myFunction() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
            var str = document.getElementById("switchPass").innerHTML; 
            var res = str.replace("Lihat", "Sembunyikan");
            document.getElementById("switchPass").innerHTML = res;
        } else {
            x.type = "password";
            var str = document.getElementById("switchPass").innerHTML; 
            var res = str.replace("Sembunyikan", "Lihat");
            document.getElementById("switchPass").innerHTML = res;
        }
    }

    $("#form-update").submit(function () {
    $(".loader").addClass("active");
        var formData = new FormData($(this)[0]);
        var form = $(this);
        $.ajax({
            url: form.attr('action'), 
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            async: false,
            processData: false,
            success: function (data) {
                console.log(data);
                test = data;
                $(".loader").removeClass("active");
                if (data.status == 1) {
                    swal("Terima Kasih!", "Data Berhasil di Update", "success").then((value) => {
                    switch (value) {
                        default:
                        location.reload();
                        break;
                    }
                    });
                }
                else {
                    swal("Gagal!", "Coba Periksa Lagi", "error");
                }
            }
        });
    });
</script>
@endsection