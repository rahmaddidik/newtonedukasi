<?php

class Panel_users extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->module = str_replace('panel_', '', strtolower( get_class($this) ) );
        date_default_timezone_set('Asia/Jakarta');

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('panel/auth');
        }

        if (!$this->ion_auth->is_admin())
        {
            // redirect them to the login page
            redirect('panel');
        }

        $this->load->model('users');
    }

    public function index()
    {

        view_back( "panel_$this->module/views/v_users", [], __DIR__);
    }


//    get data
    public function get()
    {

        $users = $this->users->get();

        if (!$this->input->is_ajax_request()) {
            $data = [
                'status' => 0,
                'message' => validation_errors(),
            ];
            exit('No direct script access allowed');
        } else {
            if ($this->input->server('REQUEST_METHOD') == 'GET') {
                $data = [
                    'status' => 1,
                    'message' => 'Data Berhasil Di Tampilkan',
                    'data' => $users->result_array()
                ];
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    add data
    public function add()

    {

        if (!$this->input->is_ajax_request()) {

            exit('No direct script access allowed');

        } else {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data_group=(int)$this->input->post('jabatan');
                $data=[
                    'name'=>$this->input->post('name'),
                    'phone'=>$this->input->post('phone'),
                    'address'=>$this->input->post('address'),
                ];

                if($data_group === 1){
                    $data_groups=[1,2];

                }else{
                    $data_groups=[(int)$this->input->post('jabatan')];
                }

                $insert_berhasil = $this->ion_auth->register($this->input->post('email'),$this->input->post('password'),$this->input->post('email'),$data,$data_groups);
                if ($insert_berhasil) {
                    $data = [
                        'status' => 1,
                        'message' => 'Data Berhasil Di Input'
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'message' => validation_errors(),
                    ];
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    edit data
    public function edit($id)
    {

        if (!$this->input->is_ajax_request()) {
            $data = [
                'status' => 0,
                'message' => validation_errors(),
            ];
            exit('No direct script access allowed');
        } else {
            $pencarian_berhasil = $this->users->get_by_id($id);
            if ($this->input->server('REQUEST_METHOD') == 'GET') {
                $data = [
                    'status' => 1,
                    'message' => 'Data Berhasil Di Tampilkan',
                    'data' => $pencarian_berhasil->result_array()
                ];
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    update data
    public function update($id)
    {
        if (!$this->input->is_ajax_request()) {

            exit('No direct script access allowed');

        } else {

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $data_group=(int)$this->input->post('jabatan');

                $data=[
                    'name'=>$this->input->post('name'),
                    'phone'=>$this->input->post('phone'),
                    'address'=>$this->input->post('address'),
                ];

                if($data_group === 1){

                    $data_groups=[1,2];

                }else{
                    $data_groups=[(int)$this->input->post('jabatan')];
                }

                $delete_berasil = $this->ion_auth_model->delete_user($id);

                if($delete_berasil)
                $insert_berhasil = $this->ion_auth->register($this->input->post('email'),$this->input->post('password'),$this->input->post('email'),$data,$data_groups);
                if ($insert_berhasil) {
                    $data = [
                        'status' => 1,
                        'message' => 'Data Berhasil Di Update'
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'message' => validation_errors(),
                    ];
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

//    delete data
    public function delete($id)
    {

        if (!$this->input->is_ajax_request() and false) {
            $data = [
                'status' => 0,
                'message' => validation_errors(),
            ];
            exit('No direct script access allowed');
        } else {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $delete_berasil = $this->ion_auth_model->delete_user($id);
                if ($delete_berasil) {
                    $data = [
                        'status' => 1,
                        'message' => 'Data Berhasil Di Delete'
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'message' => validation_errors(),
                    ];
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}