<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

  <div class="navbar-header">
    <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided" data-toggle="menubar">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-bar"></span>
    </button>
    <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
      <i class="icon wb-more-horizontal" aria-hidden="true"></i>
    </button>
    <div class="navbar-brand navbar-brand-center">
      <img class="navbar-brand-logo" src="{{assets_back()}}base/assets/images/logo.png">
      <span class="navbar-brand-text hidden-xs-down"> Newton Edukasi</span>
    </div>
  </div>

  <div class="navbar-container container-fluid">
    <!-- Navbar Collapse -->
    <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
      <!-- Navbar Toolbar -->
      <ul class="nav navbar-toolbar">
        <li class="nav-item hidden-float" id="toggleMenubar">
          <a class="nav-link" data-toggle="menubar" href="#" role="button">
            <i class="icon hamburger hamburger-arrow-left">
              <span class="sr-only">Toggle menubar</span>
              <span class="hamburger-bar"></span>
            </i>
          </a>
        </li>
        <li class="nav-item hidden-sm-down" id="toggleFullscreen">
          <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
            <span class="sr-only">Toggle fullscreen</span>
          </a>
        </li>
      </ul>
      <!-- End Navbar Toolbar -->

      <!-- Navbar Toolbar Right -->
      <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
        <li class="nav-item dropdown">
          <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
            <span class="avatar avatar-online">
              <img src="{{assets_back()}}global/portraits/5.jpg" alt="...">
              <i></i>
            </span>
          </a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
              <i class="icon wb-user" aria-hidden="true"></i> Profil</a>
            <div class="dropdown-divider" role="presentation"></div>
            <a class="dropdown-item" href="{{ base_url('panel/auth/logout') }}" role="menuitem">
              <i class="icon wb-power" aria-hidden="true"></i> Keluar</a>
          </div>
        </li>
      </ul>
      <!-- End Navbar Toolbar Right -->
    </div>
    <!-- End Navbar Collapse -->
  </div>
</nav>

<div class="site-menubar">
  <div class="site-menubar-body">
    <div>
      <div>
        <ul class="site-menu" data-plugin="menu">
          <li class="site-menu-category">MENU</li>
          <li class="site-menu-item <?=(current_url()==base_url('panel')) ? 'active':''?>">
            <a class="animsition-link" href="{{ base_url('panel') }}">
              <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
              <span class="site-menu-title">Dashboard</span>
            </a>
          </li>
          <li class="site-menu-item <?=(current_url()==base_url('panel/users')) ? 'active':''?>">
            <a class="animsition-link" href="{{ base_url('panel/users') }}">
              <i class="site-menu-icon wb-users" aria-hidden="true"></i>
              <span class="site-menu-title">Users</span>
            </a>
          </li>
          <li class="site-menu-item has-sub <?=(current_url()==base_url('panel/kategori_buku')) || (current_url()==base_url('panel/data_diskon')) || (current_url()==base_url('panel/data_buku')) || (current_url()==base_url('panel/data_pemasok')) || (current_url()==base_url('panel/data_relasi')) ? 'active open':''?>">
            <a href="javascript:void(0)">
              <i class="site-menu-icon wb-briefcase" aria-hidden="true"></i>
              <span class="site-menu-title">Master Data</span>
              <span class="site-menu-arrow"></span>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item <?=(current_url()==base_url('panel/kategori_buku')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/kategori_buku') }}">
                  <span class="site-menu-title">Kategori Buku</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/data_diskon')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/data_diskon') }}">
                  <span class="site-menu-title">Data Diskon</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/data_buku')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/data_buku') }}">
                  <span class="site-menu-title">Data Buku</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/data_pemasok')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/data_pemasok') }}">
                  <span class="site-menu-title">Data Pemasok</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/data_relasi')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/data_relasi') }}">
                  <span class="site-menu-title">Data Relasi</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="site-menu-item has-sub <?=(current_url()==base_url('panel/penjualan')) || (current_url()==base_url('panel/buku_masuk')) || (current_url()==base_url('panel/retur_masuk')) ? 'active open':''?>">
            <a href="javascript:void(0)">
              <i class="site-menu-icon wb-shopping-cart" aria-hidden="true"></i>
              <span class="site-menu-title">Transaksi</span>
              <span class="site-menu-arrow"></span>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item <?=(current_url()==base_url('panel/penjualan')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/penjualan') }}">
                  <span class="site-menu-title">Penjualan</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/buku_masuk')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/buku_masuk') }}">
                  <span class="site-menu-title">Buku Masuk</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/retur_masuk')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/retur_masuk') }}">
                  <span class="site-menu-title">Retur Buku Masuk</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="site-menu-item has-sub <?=(current_url()==base_url('panel/faktur_bkp')) || (current_url()==base_url('panel/faktur_bkp/cetak')) || (current_url()==base_url('panel/faktur_dtp')) || (current_url()==base_url('panel/faktur_alquran')) || (current_url()==base_url('panel/faktur_khusus')) || (current_url()==base_url('panel/retur_penerbit')) ? 'active open':''?>">
            <a href="javascript:void(0)">
              <i class="site-menu-icon wb-tag" aria-hidden="true"></i>
              <span class="site-menu-title">Faktur Buku Keluar</span>
              <span class="site-menu-arrow"></span>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item <?=(current_url()==base_url('panel/faktur_bkp')) || (current_url()==base_url('panel/faktur_bkp/cetak')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/faktur_bkp') }}">
                  <span class="site-menu-title">BKP Gramedia</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/faktur_dtp')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/faktur_dtp') }}">
                  <span class="site-menu-title">DTP Gramedia</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/faktur_alquran')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/faktur_alquran') }}">
                  <span class="site-menu-title">ALQURAN Gramedia</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/faktur_khusus')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/faktur_khusus') }}">
                  <span class="site-menu-title">Khusus</span>
                </a>
              </li>
              <li class="site-menu-item <?=(current_url()==base_url('panel/retur_penerbit')) ? 'active':''?>">
                <a class="animsition-link" href="{{ base_url('panel/retur_penerbit') }}">
                  <span class="site-menu-title">Retur ke Penerbit</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="site-menu-item has-sub">
            <a href="javascript:void(0)">
              <i class="site-menu-icon wb-print" aria-hidden="true"></i>
              <span class="site-menu-title">Laporan</span>
              <span class="site-menu-arrow"></span>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item">
                <a class="animsition-link" href="{{ base_url('panel/data_faktur') }}">
                  <span class="site-menu-title">Data Faktur</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>

</div>